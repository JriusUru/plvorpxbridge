// dllmain.cpp : DLL entry point

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

#include "pnFactory/plCreator.h"
#include "plResMgr/plResManager.h"
#include "plTimerCallbackManager.h"
#include "hsTimer.h"
#include "plNetClient/plNetClientMgr.h"

#include "plExternalCreatableIndex.h"
#include "plVorpxBridge.h"

std::ofstream logger("ModDLL\\VorpxBridge.log");

/*
EXPLANATION of all this mess:

We want this plugin to override camera position at runtime just before rendering.

We're compiling to a DLL, which will be placed in the ModDLL folder. Those are auto-loaded,
and InitGlobals is automatically called with a few pointers to often-used classes.

In order to do stuff at runtime, we need to create a custom KeyedObject. This object will be able
to receive Plasma messages for a whole slew of stuff, and be notified of many events, as well
as receive pointers to useful objects (such as the plPipeline in use).

In order to create that custom KeyedObject, we have to register it in the plFactory using various macros.
The problem is that those are somewhat outdated, so issues may arise.

*/



// copy-pasted+fixed from DECLARE_EXTERNAL_CREATABLE. Does not rely on VERIFY_CREATABLE, which uses internal class index.
#define DECLARE_EXTERNAL_CREATABLE_FIXED( plClassName )                             \
                                                                                    \
class plClassName##__Creator : public plCreator                                     \
{                                                                                   \
public:                                                                             \
    plClassName##__Creator()                                                        \
    {                                                                               \
    }                                                                               \
    virtual ~plClassName##__Creator()                                               \
    {                                                                               \
        plFactory::UnRegister(EXTERN_CLASS_INDEX_SCOPED(plClassName), this);        \
    }                                                                               \
    void Register()                                                                 \
    {                                                                               \
        plFactory::Register( EXTERN_CLASS_INDEX_SCOPED(plClassName), this);         \
        plClassName::SetClassIndex(ClassIndex());                                   \
    }                                                                               \
    void UnRegister()                                                               \
    {                                                                               \
        plFactory::UnRegister(EXTERN_CLASS_INDEX_SCOPED(plClassName), this);        \
    }                                                                               \
                                                                                    \
    bool HasBaseClass(uint16_t hBase) override { return plClassName::HasBaseClass(hBase); }  \
                                                                                    \
    uint16_t ClassIndex() override { return EXTERN_CLASS_INDEX_SCOPED(plClassName); }  \
    const char* ClassName() const override { return #plClassName; }                 \
                                                                                    \
    plCreatable* Create() const override { return new plClassName; }                \
                                                                                    \
};                                                                                  \
static plClassName##__Creator   static##plClassName##__Creator;                     \
uint16_t plClassName::plClassName##ClassIndex = 0;                                  \
                                                                                    \
static_assert(plCreator::VerifyKeyedIndex<plClassName, EXTERN_CLASS_INDEX_SCOPED(plClassName)>(),    \
              #plClassName " is in the KeyedObject section of plCreatableIndex but "          \
              "does not derive from hsKeyedObject.");                                         \
                                                                                              \
static_assert(plCreator::VerifyNonKeyedIndex<plClassName, EXTERN_CLASS_INDEX_SCOPED(plClassName)>(), \
              #plClassName " is in the non-KeyedObject section of plCreatableIndex but "      \
              "derives from hsKeyedObject.");

// Register the plVorpxBridge in the plFactory.
DECLARE_EXTERNAL_CREATABLE_FIXED(plVorpxBridge);

/// <summary>
/// Boring Windows DLL attachment stuff.
/// </summary>
BOOL APIENTRY DllMain(HMODULE hModule,
    DWORD  ul_reason_for_call,
    LPVOID lpReserved)
{
    switch (ul_reason_for_call)
    {
    case DLL_PROCESS_ATTACH:
    case DLL_THREAD_ATTACH:
    case DLL_THREAD_DETACH:
        break;
    case DLL_PROCESS_DETACH:
        // Not sure how often this is called, but Kahlo source code indicates we should do the following.
        UNREGISTER_EXTERNAL_CREATABLE(plVorpxBridge);

        plVorpxBridge::SetInstance(nullptr);
        hsTimer::SetTheTimer(nullptr);
        plgTimerCallbackMgr::SetTheTimerCallbackMgr(nullptr);
        plFactory::SetTheFactory(nullptr);
        hsgResMgr::Shutdown();
        if (plNetClientApp::GetInstance())
            plNetClientApp::GetInstance()->Shutdown();

        break;
    }
    return TRUE;
}

extern "C"
{
    /// <summary>
    /// Called by the plClient after our DLL is loaded.
    /// Creates a plVorpxBridge, which will hax the pipeline to override camera positions.
    /// </summary>
    __declspec(dllexport) void __cdecl InitGlobals(
        hsResMgr* resMgr,
        plFactory* factory,
        plTimerCallbackManager* timerCallbackManager,
        plTimerShare* timerShare,
        plNetClientApp* netClientApp)
    {
        logger << "Init Vorpx bridge" << std::endl;
        logger << "Res manager: " << resMgr << std::endl;
        logger << "Factory: " << factory << std::endl;
        logger << "Timer callback manager: " << timerCallbackManager << std::endl;
        logger << "Timer share: " << timerShare << std::endl;
        logger << "Net client app: " << netClientApp << std::endl;
        logger.flush();

        logger << "Initing stuff..." << std::endl;
        logger.flush();

        // Set the static (global) singletons, because our DLL doesn't share the global scope with the engine.
        hsgResMgr::Init(resMgr);
        plFactory::SetTheFactory(factory);
        plgTimerCallbackMgr::SetTheTimerCallbackMgr(timerCallbackManager);
        hsTimer::SetTheTimer(timerShare);
        plNetClientApp::SetInstance(netClientApp);

        logger << "Stuff inited correctly. Registering plVorpxBridge..." << std::endl;

        REGISTER_EXTERNAL_CREATABLE(plVorpxBridge)

        logger << "Registered. Creating bridge..." << std::endl;

        // Now create the Vorpx bridge.
        plVorpxBridge* vorpx = plVorpxBridge::Create();
        resMgr->NewKey("VorpxBridge", vorpx, plLocation::kGlobalFixedLoc);
        logger << "Bridge created, linking singleton..." << std::endl;
        plVorpxBridge::SetInstance(vorpx);
        logger << "Singleton OK" << std::endl;
        plKey vorpxKey = vorpx->GetKey();
        logger << "Got key" << std::endl;
        logger << "    Key name = " << vorpxKey->GetName().c_str() << std::endl;
        logger << "    Class index=" << vorpxKey->GetObjectPtr()->ClassIndex() << std::endl;
        logger << "    Class name=" << vorpxKey->GetObjectPtr()->ClassName() << std::endl;

        logger << "Done. Check other log for follow-up." << std::endl;
    }
}
