#ifndef plVorpxBridgeExternalCreatableIndex_inc
#define plVorpxBridgeExternalCreatableIndex_inc

#include "plCreatableIndex.h"

/// <summary>
/// Stores ids for our added Creatable classes.
/// </summary>
class plExternalCreatableIndex
{
public:
	enum
	{
		// Wait, so two DLLs adding new creatable objects will immediately have conflicting IDs with
		// the current system ? Ok O.o...

		// I have my doubts about this - it seems MOUL added MANY keyed objects,
		// and EXTERNAL_KEYED_DLL_BEGIN is now too low.
		// Should probably start around 318. Or even as high as 400 for safety...
		//EXTERNAL_CREATABLE_KEYED_INDEX_START = EXTERNAL_KEYED_DLL_BEGIN,
		EXTERNAL_CREATABLE_KEYED_INDEX_START = 400,

		// add your external dll indices here for keyed objects
		EXTERN_CLASS_INDEX(plVorpxBridge),

		EXTERNAL_CREATABLE_KEYED_INDEX_END = EXTERNAL_KEYED_DLL_END,
		EXTERNAL_CREATABLE_NONKEYED_INDEX_START = EXTERNAL_NONKEYED_DLL_BEGIN,

		// add your external dll indices here for creatable (nonkeyed) objects, such as messages

		EXTERNAL_CREATABLE_NONKEYED_INDEX_END = EXTERNAL_NONKEYED_DLL_END,
	};
};

#endif // plVorpxBridgeCreatableIndex_inc
