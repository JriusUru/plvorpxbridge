#include "plVorpxBridge.h"
#include "plMessage/plTimerCallbackMsg.h"
#include "plgDispatch.h"
#include "plPipeline.h"
#include "plResMgr/plResManager.h"
#include "plMessage/plRenderMsg.h"
#include "pnKeyedObject/plFixedKey.h"
#include "pnKeyedObject/plUoid.h"

plVorpxBridge* plVorpxBridge::fInstance;
std::ofstream plVorpxBridge::loggerInstance("ModDLL\\plVorpxBridge.log");

plVorpxBridge::plVorpxBridge() : virtualCam(nullptr)
{
    loggerInstance << "Vorpx bridge instance created" << std::endl;
}

plVorpxBridge::~plVorpxBridge()
{

}

void plVorpxBridge::SetKey(plKey k)
{
    hsKeyedObject::SetKey(k);
    if (k)
    {
        plgDispatch::Dispatch()->RegisterForExactType(plRenderMsg::Index(), GetKey());
    }
}

bool plVorpxBridge::MsgReceive(plMessage* msg)
{
    plRenderMsg* rend = plRenderMsg::ConvertNoRef(msg);
    if (rend)
    {
        if (virtualCam == nullptr)
        {
            plKey virtualCamKey = hsgResMgr::ResMgr()->FindKey(plUoid(plFixedKeyId::kVirtualCamera1_KEY));
            if (virtualCamKey)
            {
                virtualCam = plVirtualCam1::ConvertNoRef(virtualCamKey->GetObjectPtr());
                if (virtualCam != nullptr)
                {
                    loggerInstance << "Virtual cam found at " << virtualCam << " ! Hooking..." << std::endl;
                    loggerInstance << "Class name: " << virtualCam->ClassName() << std::endl;
                    loggerInstance << "Pipeline: " << rend->Pipeline() << std::endl;
                    virtualCam->SetPipeline(rend->Pipeline());
                }
                else
                    loggerInstance << "No virtual cam yet" << std::endl;
            }
        }

        return true;
    }

    return hsKeyedObject::MsgReceive(msg);
}
