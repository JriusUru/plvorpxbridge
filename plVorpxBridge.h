#ifndef PLVORPXBRIDGE_H_INCLUDED
#define PLVORPXBRIDGE_H_INCLUDED

#include "pnKeyedObject/hsKeyedObject.h"
#include "pfCamera/plVirtualCamNeu.h"
#include "plPipeline.h"

#include <fstream>

class plVorpxBridge : public hsKeyedObject
{
protected:
    static plVorpxBridge* fInstance;
    static std::ofstream loggerInstance;

    plVirtualCam1* virtualCam;
    plPipeline* pipeline;

    void SetKey(plKey k) override;

public:
    CLASSNAME_REGISTER(plVorpxBridge);
    GETINTERFACE_ANY(plVorpxBridge, hsKeyedObject);

    plVorpxBridge();
    ~plVorpxBridge();

    static plVorpxBridge* GetInstance() { return plVorpxBridge::ConvertNoRef(fInstance); }
    static const plVorpxBridge* GetConstInstance() { return plVorpxBridge::ConvertNoRef(fInstance); }
    static void SetInstance(plVorpxBridge* vorpx) { fInstance = vorpx; }

    bool MsgReceive(plMessage* msg) override;
};

#endif // PLVORPXBRIDGE_H_INCLUDED
